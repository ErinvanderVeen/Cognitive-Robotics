\section{Iris Flower Data Set}
\subsection{Software Design}
The neural network consists of three different kinds of neurons: input, output, and training neurons. Marked with I, O and T respectively in Figure \ref{fig:flower_network}.

\begin{figure}
    \centering
    \begin{tikzpicture}[shorten >=1pt,->,draw=black!50, node distance=\layersep]
        \tikzstyle{every pin edge}=[<-,shorten <=1pt]
        \tikzstyle{neuron}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]
        \tikzstyle{input neuron}=[neuron, fill=green!50];
        \tikzstyle{output neuron}=[neuron, fill=red!50];
        \tikzstyle{training neuron}=[neuron, fill=blue!50];
        \tikzstyle{annot} = [text width=4em, text centered]
    
        % Draw the input layer nodes
        \foreach \name / \y in {Sepal Length/1,Sepal Width/2,Petal Length/3,Petal Width/4}
            \node[input neuron, pin=left:\name] (I-\y) at (0,-\y) {I};
    
        % Draw the output layer nodes
        \foreach \name / \y in {Setosa/1,Versicolor/2,Virginica/3}
            \path[yshift=-0.5cm]
                node[output neuron, pin=right:\name] (O-\y) at (3*\layersep,-\y cm) {O};
    
        % Draw the training layer nodes
        \foreach \name / \y in {Setosa/1,Versicolor/2,Virginica/3}
            \path[yshift=3.0cm]
                node[training neuron, pin=left:\name] (T-\y) at (1.5*\layersep,-\y cm) {T};
    
        % Connect every node in the input layer with every node in the output layer.
        \foreach \source in {1,...,4}
            \foreach \dest in {1,...,3}
                \path (I-\source) edge (O-\dest);
    
        % Connect every node in the training layer with the output layer
        \foreach \source in {1,...,3}
            \path (T-\source) edge (O-\source);
    
    \end{tikzpicture}
    \caption{Iris flower data set neural network}
    \label{fig:flower_network}
\end{figure}

The program starts by reading values from the Iris flower data set file which we created beforehand. In our design we decided that the input neurons do not contain information on what data must be read from the file. This allows us to implement a generic input neuron class which can be reused for the robot later. As a result we need a way to pass the read values to the input neurons so that the data can be used for the learning process. We realized this by making every input neuron ask the HebbianLearning class for its value. The HebbianLearning class contains a map that maps an input neuron to the read data. When one of the input neurons asks for the read data, the value is simply read from the map, and returned. If we were to adapt this to the robot we would only have to change the map instead of the input neuron class. 

Training neurons are no different from input neurons. In fact, in the code they share the same class. The only thing that is different between the two, is that the weights are initially different. The training neurons are also disabled once the training phase is over by simply always returning an input of 0. This allows us to easily switch between training and non-training phases because the file input is always hooked up to the training neurons while non-training input neurons are hooked up to user input. This user input is by default an entry from the Iris flower data set since it makes testing easier but could potentially be hooked up to text input from a user.

Input neurons are an implementation of the neuron interface, as is the Neuron class. The Neuron class is a general neuron that contains all calculations that are typically performed by a neuron. It was meant for two distinct uses: extension by OutputNeuron, and on its own as a hidden layer neuron. During development we decided to drop the hidden layer that was once between the input and output layer because our implementation wasn't working. We redesigned the code base and felt that there was no need for a hidden layer. As such, the usage of the Neuron class is reduced to one thing which is to be extended by OutputNeuron.

The OutputNeuron is a typical neuron with the addition of a ``name'' property. The name property allows the program to display what its final decision of a flower is. It also serves as a bridge between the FlowerDataSet enumeration and the neurons themselves. In hindsight we would rather have used some kind of reference but didn't feel strongly enough about it to actually make the change as it would make no difference to the results of our artificial neural network.

The calculations are done in such a way as to avoid useless calculations. We do not iterate over every neuron and calculate it's value. Instead, the value of a neuron is only calculated when it is needed. It is then stored for when it is needed again. To begin the calculation, one would have to call the ``output()'' function on an output neuron. Said neuron will then ask all of it's parents what their value is. These parents will, in turn, ask their parents, etc. While this method of calculation is still $\mathcal{O}(n)$, with $n$ the number of neurons, it has a much better best case complexity of just $\mathcal{O}(1)$. The na\"ive approach would require $\mathcal{O}(n)$, even for the best case. It also eliminates the need to pick a correct order to calculate the neurons.

During the training section we provide the training neurons with a value of 1 or -1. We give it 1 when the neuron represents the flower that is currently considered, and -1 if it does not. The -1 and 1, in combination with a weight of 100, makes sure that the correct node fires in every case. This, in turn, strengthens the connections between neurons that represent this flower.

All weights are initialized with 1, with the aforementioned exception of 100 for the training neurons. We experimented with several activation functions, but settled on the ARCTAN. All functions can be found in the ``ActivationFunction'' enumeration. This enumeration is also responsible for making the correct calculations. A better solution would have been to provide the neurons with a function, but the Java version of the leJOS that was installed on the EV3 didn't support said feature.

Since a simple implementation of Hebbian Learning results in so called weight explosion we implemented weight normalization by using z-score. By using this normalization technique we take into account how far values are from the mean in terms of standard deviations which makes sure a value does not go too far away from the mean. The reason for using this technique was because it was recommended in the assignment and functioned well as can be seen in the results section.

\subsection{Results}
As mentioned before the network is trained on the Iris flower data set. Once the training phase is over the artificial neural network is given random entries from the Iris flower data set from which it returns the species. In our experience the accuracy was slightly higher than 80\% after tweaking our activation function, activation threshold, and learning rate. The assignment mentions that we should be able to reach an accuracy of around 95\% but we did not manage to reach that.

When testing the non-normalized version we were only able to reach an accuracy of around 35\% which is almost equivalent to a random guess. We were not able to figure out the exact reason why this was the case but we think it might have to do with the enormous weight explosion. Because of the weight explosion after 3 unique species pretty much every weight has become very high. The rest of the flower samples will thus cause all output neurons to fire since the weights are far above the threshold which in turn reinforces all connections regardless of what species we are trying to train. Since weight normalization makes sure that the weights do not become excessively large this problem is averted because not all weights will be above the activation threshold.

Something we have experimented with is learning multiple times on the same data set instead of once. As far as accuracy goes it had no effect since we always stayed slightly above 80\% it did make the results more stable. If we only learned once on the data set we would sometimes get 81\% while other times we would get 83\%. If we learned multiple times we would always get 82\%, so we decided to learn the data set 10 times.

Even though our accuracy was around 80\% it does mean that the artificial neural network can do significantly better than a random guess which is exactly what we want. Since we had time constraints and our results were reasonable we decided to continue working on our robot instead of the Iris flower data set. In the next section we can indeed see that our artificial neural network performed well enough to be used in our robot.