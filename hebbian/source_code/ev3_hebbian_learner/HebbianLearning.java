import java.util.ArrayList;

import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.BaseSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.NXTUltrasonicSensor;

public class HebbianLearning {
	
	private ArrayList<OutputNeuron> outputNeurons;
	private static InputNeuron touchL, distanceL, touchR, distanceR;
	private static BaseSensor sensorTouchL, sensorDistanceL, sensorTouchR, sensorDistanceR;
	
	public static float readValue(InputNeuron neuron) {
		float[] samples;
		if (neuron == touchL) {
			samples = new float[sensorTouchL.sampleSize()];
			sensorTouchL.fetchSample(samples, 0);
			return samples[0];
		} else if (neuron == distanceL){
			samples = new float[sensorDistanceL.sampleSize()];
			sensorDistanceL.fetchSample(samples, 0);
			return 1 - Math.min(samples[0], 1.0f);
		} else if (neuron == touchR) {
			samples = new float[sensorTouchR.sampleSize()];
			sensorTouchR.fetchSample(samples, 0);
			return samples[0];
		} else if (neuron == distanceR) {
			samples = new float[sensorDistanceR.sampleSize()];
			sensorDistanceR.fetchSample(samples, 0);
			return 1 - Math.min(samples[0], 1.0f);
		}
		return Float.NaN; //aNaNaNa BATMAN
	}
	
	private void iteration() {
		for (OutputNeuron e : outputNeurons) {
			e.run();
			e.reset();
		}
	}
	
	private void createNetwork() {
		float activationThreshold = 0.75f;
		float learningRate = 0.1f;
		ActivationFunction af = ActivationFunction.ARCTAN;
		
		ArrayList<InputNeuron> IL = new ArrayList<InputNeuron>();
		ArrayList<InputNeuron> IR = new ArrayList<InputNeuron>();
		
		touchL = new InputNeuron();
		distanceL = new InputNeuron();
		touchR = new InputNeuron();
		distanceR = new InputNeuron();
		
		IL.add(touchL);
		IL.add(distanceL);
		IR.add(touchR);
		IR.add(distanceR);
		
		OutputNeuron motorL = new OutputNeuron(activationThreshold, learningRate, af, MotorPort.A);
		OutputNeuron motorR = new OutputNeuron(activationThreshold, learningRate, af, MotorPort.D);
		
		outputNeurons.add(motorL);
		outputNeurons.add(motorR);
		
		motorL.addParent(IR.get(0), 1.0f);
		motorL.addParent(IR.get(1), 0.0f);
		
		motorR.addParent(IL.get(0), 1.0f);
		motorR.addParent(IL.get(1), 0.0f);
		
		loadSensors();
	}
	
	private void loadSensors() {
		sensorTouchL = new EV3TouchSensor(SensorPort.S2);
		sensorDistanceL = new EV3UltrasonicSensor(SensorPort.S1);
		sensorTouchR = new EV3TouchSensor(SensorPort.S3);
		sensorDistanceR = new NXTUltrasonicSensor(SensorPort.S4);
		
	}

	public HebbianLearning() {
		createNetwork();
	}
	
	public static void main(String[] args) {
		HebbianLearning hebbLearner = new HebbianLearning();
		while (true) {
			hebbLearner.iteration();
		}
	}
}
