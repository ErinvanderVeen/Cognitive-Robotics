import java.util.ArrayList;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;

public class OutputNeuron extends Neuron {
	private EV3LargeRegulatedMotor motor;
	private Port motorport;
	
	public OutputNeuron(float activationThreshold, float learningRate,
			ActivationFunction activationFunction, Port motorport) {
		super(activationThreshold, learningRate, activationFunction);
		this.motorport = motorport;
		motor = new EV3LargeRegulatedMotor(motorport);
	}
	
	@Override
	public String toString() {
		return motorport.toString();
	}
	
	public void run() {
		float output = super.output();
		motor.setSpeed((1 - output) * motor.getMaxSpeed());
	}
}
