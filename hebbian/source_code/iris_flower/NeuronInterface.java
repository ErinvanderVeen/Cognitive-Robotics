public interface NeuronInterface {
	public float output();
	public void reset();
	public String getWeights();
	public void addParent(NeuronInterface parent, float weight);
}
