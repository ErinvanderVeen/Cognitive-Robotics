public enum ActivationFunction {
	IDENTITY, BINARY_STEP, LOGISTIC, TANH, ARCTAN, SOFTSIGN, RECTIFIER, GAUSSIAN;

	public static float function(float input, ActivationFunction function) {
		switch (function) {
			case BINARY_STEP:
				return input >= 1.0f ? 1 : 0;
			case LOGISTIC:
				return (float) (1.0f / (1.0f + Math.pow(Math.E, input)));
			case TANH:
				return (float) Math.tanh(input);
			case ARCTAN:
				return (float) Math.atan(input);
			case SOFTSIGN:
				return input / (1.0f + Math.abs(input));
			case RECTIFIER:
				return Math.max(input, 0);
			case GAUSSIAN:
				return (float) Math.pow(Math.E, Math.pow(-input, 2));
			default:
				return input;
		}
	}
}
