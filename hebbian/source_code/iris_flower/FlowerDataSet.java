import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlowerDataSet {
	public enum FlowerClass {
		SETOSA,
		VERSICOLOR,
		VIRGINICA;
		
		public static FlowerClass fromString(String flowerClass) {
			switch(flowerClass) {
				case "I. setosa":
					return SETOSA;
				case "I. versicolor":
					return VERSICOLOR;
				case "I. virginica":
					return VIRGINICA;
				default:
					return null;
			}
		}
	}
	
	public class FlowerDataLine {
		public float sepalLength;
		public float sepalWidth;
		public float petalLength;
		public float petalWidth;
		public FlowerClass knownClass;
	}
	
	private List<FlowerDataLine> listFDL = new ArrayList<>();
	private List<FlowerDataLine> initialListFDL;
	
	public FlowerDataSet(Path filePath) {
		try {
			List<String> lines = Files.readAllLines(filePath, Charset.forName("UTF-8"));
			for (String line : lines) {
				String[] lineParts = line.split(",");
				FlowerDataLine fdl = new FlowerDataLine();
				fdl.sepalLength = Float.parseFloat(lineParts[0]);
				fdl.sepalWidth = Float.parseFloat(lineParts[1]);
				fdl.petalLength = Float.parseFloat(lineParts[2]);
				fdl.petalWidth = Float.parseFloat(lineParts[3]);
				fdl.knownClass = FlowerClass.fromString(lineParts[4]);
				listFDL.add(fdl);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		initialListFDL = new ArrayList<>(listFDL);
	}
	
	public void normalize() {
		float[] means = new float[4];
		float[] deviations = new float[4];
		
		for (FlowerDataLine fdl : initialListFDL) {
			means[0] += fdl.sepalLength / initialListFDL.size();
			means[1] += fdl.sepalWidth / initialListFDL.size();
			means[2] += fdl.petalLength / initialListFDL.size();
			means[3] += fdl.petalWidth / initialListFDL.size();
		}
		
		for (FlowerDataLine fdl : initialListFDL) {
			deviations[0] += Math.pow(fdl.sepalLength - means[0], 2.0f) / initialListFDL.size();
			deviations[1] += Math.pow(fdl.sepalWidth - means[1], 2.0f) / initialListFDL.size();
			deviations[2] += Math.pow(fdl.petalLength - means[2], 2.0f) / initialListFDL.size();
			deviations[3] += Math.pow(fdl.petalWidth - means[3], 2.0f) / initialListFDL.size();
		}
		
		for (int i = 0; i < 4; i++) {
			deviations[i] = (float) Math.sqrt(deviations[i]);
		}
		
		for (FlowerDataLine fdl : initialListFDL) {
			fdl.sepalLength = (fdl.sepalLength - means[0]) / deviations[0];
			fdl.sepalWidth = (fdl.sepalWidth - means[1]) / deviations[1];
			fdl.petalLength = (fdl.petalLength - means[2]) / deviations[2];
			fdl.petalWidth = (fdl.petalWidth - means[3]) / deviations[3];
		}
	}
	
	public FlowerDataLine getRandomEntryAndRemove() {
		if (listFDL.isEmpty())
			return null;
		
		FlowerDataLine entry = listFDL.get(new Random().nextInt(listFDL.size()));
		listFDL.remove(entry);
		return entry;
	}
	
	public void reset() {
		listFDL = new ArrayList<>(initialListFDL);
	}
}
