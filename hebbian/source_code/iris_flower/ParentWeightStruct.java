public class ParentWeightStruct {
	public NeuronInterface parent;
	public float weight;
	
	public ParentWeightStruct(NeuronInterface parent, float weight) {
		this.parent = parent;
		this.weight = weight;
	}
}
