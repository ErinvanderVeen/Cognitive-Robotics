import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HebbianLearning {
	private static Map<InputNeuron, Float> map = new HashMap<>();
	private boolean isCurrentlyTraining = true;
	private FlowerDataSet fds;
	private static int correct = 0;

	private InputNeuron sepalLength = new InputNeuron();
	private InputNeuron sepalWidth = new InputNeuron();
	private InputNeuron petalLength = new InputNeuron();
	private InputNeuron petalWidth = new InputNeuron();
	private InputNeuron knownClassSetosa = new InputNeuron();
	private InputNeuron knownClassVersicolor = new InputNeuron();
	private InputNeuron knownClassVirginica = new InputNeuron();
	private ArrayList<OutputNeuron> outputNeurons;
	
	public static float readValue(InputNeuron neuron) {
		return map.get(neuron);
	}
	
	private void test() {
		fds.reset();
		this.isCurrentlyTraining = false;
		
		FlowerDataSet.FlowerDataLine fdl;
		while ((fdl = fds.getRandomEntryAndRemove()) != null) {
			FlowerDataSet.FlowerClass result = iteration(fdl);
		}		
	}
	
	private void createMap(FlowerDataSet.FlowerDataLine data) {
		map.put(this.sepalLength, data.sepalLength);
		map.put(this.sepalWidth, data.sepalWidth);
		map.put(this.petalLength, data.petalLength);
		map.put(this.petalWidth, data.petalWidth);
		map.put(this.knownClassSetosa, 0.0f);
		map.put(this.knownClassVersicolor, 0.0f);
		map.put(this.knownClassVirginica, 0.0f);
		
		if (isCurrentlyTraining) {
			
			map.put(this.knownClassSetosa, -1.0f);
			map.put(this.knownClassVersicolor, -1.0f);
			map.put(this.knownClassVirginica, -1.0f);
			
			switch (data.knownClass) {
				case SETOSA:
					map.put(this.knownClassSetosa, 1.0f);
					break;
				case VERSICOLOR:
					map.put(this.knownClassVersicolor, 1.0f);
					break;
				case VIRGINICA:
					map.put(this.knownClassVirginica, 1.0f);
					break;
				default:
					break;
			}
		}
	}
	
	private FlowerDataSet.FlowerClass iteration(FlowerDataSet.FlowerDataLine data) {
		createMap(data);
		OutputNeuron max = null;
		float maxVal = Float.NEGATIVE_INFINITY;
		
		for (OutputNeuron e : outputNeurons) {
			if (e.output() > maxVal) {
				max = e;
				maxVal = e.output();
			}
		}
		if (!isCurrentlyTraining && data.knownClass == FlowerDataSet.FlowerClass.fromString(max.toString()))
			correct++;
		
		for (OutputNeuron n : outputNeurons) {
			n.reset();
		}
		
		return FlowerDataSet.FlowerClass.fromString(max.toString());
	}
	
	private void train() {
		fds.reset();
		FlowerDataSet.FlowerDataLine fdl;
		while ((fdl = fds.getRandomEntryAndRemove()) != null) {
			iteration(fdl);
		}
	}
	
	private ArrayList<OutputNeuron> createNetwork() {
		float activationThreshold = 0.75f;
		float learningRate = 0.1f;
		ActivationFunction af = ActivationFunction.ARCTAN;
		
		ArrayList<InputNeuron> I = new ArrayList<InputNeuron>();
		I.add(this.sepalLength);
		I.add(this.sepalWidth);
		I.add(this.petalLength);
		I.add(this.petalWidth);
		
		
		ArrayList<OutputNeuron> O = new ArrayList<>();
		OutputNeuron setosa = new OutputNeuron(activationThreshold, learningRate, af, "I. setosa");
		OutputNeuron versicolor = new OutputNeuron(activationThreshold, learningRate, af, "I. versicolor");
		OutputNeuron virginica = new OutputNeuron(activationThreshold, learningRate, af, "I. virginica");
		
		setosa.addParent(knownClassSetosa, 100.0f);
		versicolor.addParent(knownClassVersicolor, 100.0f);
		virginica.addParent(knownClassVirginica, 100.0f);
		
		O.add(setosa);
		O.add(versicolor);
		O.add(virginica);
		
		
		for (NeuronInterface e : I) {
			for (NeuronInterface f : O) {
				f.addParent(e, 1.0f);
			}
		}
				
		return O;
	}

	public HebbianLearning() {
		fds = new FlowerDataSet(Paths.get("src/data_set.txt"));
		fds.normalize();
		outputNeurons = createNetwork();
	}
	
	public static void main(String[] args) {
		HebbianLearning hebbLearner = new HebbianLearning();
		for (int i = 0; i < 10; i++) {
			hebbLearner.train();
		}

		hebbLearner.test();
		// Divide by 1.5 to get a percentage. (There are 150 samples)
		System.out.println(correct / 1.5);
	}
}
