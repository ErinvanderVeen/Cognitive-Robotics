public class InputNeuron implements NeuronInterface {
	private Float currentOutput;

	@Override
	public float output() {
		if (currentOutput == null)
			currentOutput = HebbianLearning.readValue(this);
		return currentOutput;
	}

	@Override
	public void reset() {
		currentOutput = null;
	}
	
	@Override
	public String getWeights() {
		return "input";
	}
	
	@Override
	public void addParent(NeuronInterface parent, float weight) {
		return;
	}
}
