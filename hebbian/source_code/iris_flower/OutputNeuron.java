import java.util.ArrayList;

public class OutputNeuron extends Neuron {
	private String name;
	
	public OutputNeuron(float activationThreshold, float learningRate,
			ActivationFunction activationFunction, String name) {
		super(activationThreshold, learningRate, activationFunction);
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
