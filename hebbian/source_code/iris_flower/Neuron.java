import java.util.ArrayList;

public class Neuron implements NeuronInterface {
	private ArrayList<ParentWeightStruct> parents = new ArrayList<ParentWeightStruct>();
	private Float currentOutput;
	private float activationThreshold;
	private float learningRate;
	private ActivationFunction activationFunction;
	
	public Neuron(float activationThreshold, float learningRate, ActivationFunction activationFunction) {
		this.activationThreshold = activationThreshold;
		this.learningRate = learningRate;
		this.activationFunction = activationFunction;
	}

	@Override
	public float output() {
		if (currentOutput == null)
			calc();
		return currentOutput;
	}
	
	private float summation() {
		float sum = 0;
		
		for (ParentWeightStruct pws : parents) {
			sum += pws.parent.output() * pws.weight;
		}
		return sum;
	}
	
	private float activation(float input) {
		return ActivationFunction.function(input, activationFunction);
	}
	
	private void modifyWeights() {
		for (ParentWeightStruct pws : parents) {
			pws.weight += learningRate * currentOutput * pws.parent.output();
		}
	}
	
	private void calc() {
		currentOutput = activationThreshold(activation(summation()));
		modifyWeights();
	}
	
	private float activationThreshold(float input) {
		return input > activationThreshold ? input : 0;
	}

	@Override
	public void reset() {
		for (ParentWeightStruct parent : parents) {
			parent.parent.reset();
		}
		currentOutput = null;
	}
	
	public String getWeights() {
		String res = "(";
		for (ParentWeightStruct parent : parents) {
			res += parent.weight + ":";
			res += parent.parent.getWeights() + " ";
		}
		return res += ")";
	}
	
	@Override
	public void addParent(NeuronInterface parent, float weight) {
		parents.add(new ParentWeightStruct(parent, weight));
	}
}
