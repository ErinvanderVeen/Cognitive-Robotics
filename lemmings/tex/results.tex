\section{Results}
%From your measurements, report how quickly on average 1, 2, & 3 Lemmings are able to sort the Lego blocks in their environment.
%Show our results
\subsection{One Robot}
We did two tests with just our robot. The results were somewhat disappointing in the beginning, only reaching some kind of clustering after about 12 minutes. Figure \ref{fig:solo1} \& \ref{fig:solo2} show these two tests. Please note that the images were taken roughly 1 minute apart. As we have mentioned before, we expected the blue blocks to end up near the wall, and the red blocks to end up clustered in the center of the arena.

\begin{figure}
    \centering
        \includegraphics[width=\textwidth]{../figures/test1.jpg}
    \caption{Solo run of our robot. Pictures taken roughly 1 minute apart.}
    \label{fig:solo1}
\end{figure}

\begin{figure}
    \centering
        \includegraphics[width=\textwidth]{../figures/test2.jpg}
    \caption{Second solo run of our robot. Pictures taken roughly 1 minute apart.}
    \label{fig:solo2}
\end{figure}

\subsubsection{Blue blocks}
We expected the blue blocks to end up near the walls of the arena. In order to measure this, we need to come up with some kind of definition. We defined ``close to the wall`` as follows:

\begin{definition}
Blocks are close to the wall if the distance between the block and the nearest wall is not more than 20 centimeters.
\end{definition}

Using this definition we can conclude that of the $25$ blue blocks, $21$ could be considered to be close to the wall after the first test. This corresponds to $84\%$.

For the second test, $80\%$ of the blue blocks was near the walls, for an average of $82\%$.

\subsubsection{Red blocks}
Again, we need some kind of definition to determine if the blocks are ``clustered''. Clustering is defined as follows:

\begin{definition}
Blocks are considered to be in the same cluster if the distance between them is no more than 10 centimeters.
\end{definition}

We can now apply this definition to our results. In test 1, $12$ out of $24$ blocks are part of a cluster. This equals $50\%$. In test two the robot managed to cluster about $66\%$ of red blocks, averaging at $58\%$.

\subsubsection{Accuracy}
In order to measure the relative performance of 1, 2 and 3 robots we must also define some kind of ``accuracy''. Dawson et al. used ``arbitrary speed units''. However, since we let our experiments run 25 minutes each, and assuming that the progress of the robots is somewhat linear it is wiser for us to define some other measure of performance.

\begin{definition}
The performance of a set of robots is the average of the percentage of blue blocks that conform to Definition 1 and the percentage of red blocks that conform to Definition 2.
\end{definition}

Applying this definition to test 1 and 2 yields a performance of $\frac{82+58}{2} = 70$.

\subsection{Two Robots}
Our lemmings were able to achieve comparable results to the ones in Figure \ref{fig:solo1} \& \ref{fig:solo2} in 25 minutes as evidenced by Figure \ref{fig:duo1}. Using the same definitions as the ones used for the experiment with the single robot we can, again, calculate the performance. Please note that we only did one experiment with two robots due to difficulties finding other groups, so the data is somewhat less accurate.

\subsubsection{Blue blocks}
$$\frac{20}{25}*100 = 80\%$$

\subsubsection{Red blocks}
$$\frac{13}{24}*100 \approx 54\%$$

\subsubsection{Accuracy}
$$\frac{80 + 54}{2} = 67\%$$
This is worse than we expected. However, like mentioned before, we assume that the work the robots do is somewhat linear in time. When one compares figure \ref{fig:solo1} and \ref{fig:solo2} with figure \ref{fig:duo1}, however, one can see that the two robots start their work somewhat faster. After a certain point the work the two robots do seems to slow down, or even halt completely.


\begin{figure}
    \centering
        \includegraphics[width=\textwidth]{../figures/test3.jpg}
    \caption{Run of two robots. Pictures show a time span of 25 minutes.}
    \label{fig:duo1}
\end{figure}

\subsection{Three Robots}
At first glance the results of the experiment with three robots (see Figure \ref{fig:triple1}) doesn't seem better (maybe even worse) than the results obtained with two robots. Again, only one experiment was done with three robots, so the data will not be as precise.

\subsubsection{Blue blocks}
$$\frac{24}{25}*100 = 96\%$$

\subsubsection{Red blocks}
$$\frac{11}{24}*100 \approx 46\%$$

\subsubsection{Accuracy}
$$\frac{96 + 54}{2} = 71\%$$
Again, lower than we expected. Possibly due to the same reason as provided in the Accuracy section of the two robot experiment.


\begin{figure}
    \centering
        \includegraphics[width=\textwidth]{../figures/test4.jpg}
    \caption{First run of three robots. Pictures taken roughly 1 minute apart.}
    \label{fig:triple1}
\end{figure}

\subsection{Collective Intelligence}
% Make a similar figure as in Dawson et al. (p. 258). Discuss whether your Lemmings have collective intelligence and why (or why not).
\begin{figure}
    \centering
    \begin{tikzpicture}[scale = 1.2]
        \begin{axis}[
            title={Number of Lemmings compared to the Performance},
            xlabel={Number of lemmings},
            ylabel={Performance as defined in Definition 3},
            xmin=0, xmax=4,
            ymin=60, ymax=80,
            xtick={0,1,2,3,4},
            ytick={60,65,70,75,80},
            legend pos=north west,
            ymajorgrids=true,
            grid style=dashed,
        ]
        \addplot[
            color=blue,
            mark=square,
            ]
            coordinates {
            (1,70)(2,67)(3,71)
            };
            \legend{Performance}
        \end{axis}
    \end{tikzpicture}
    \label{tikz:graph}
\end{figure}

As is evident from Graph \ref{tikz:graph} the speed at which the blocks get sorted does not seem to increase with the number of robots. This could be due to a variety of factors.
\begin{enumerate}
    \item Our time was limited to 25 minutes.
    \item leJOS threw exceptions at random times which seemed to impair the efficiency of the robot.
    \item The bottom sensor was far from ideal.
    \item Our assumption about the linear work over time could be wrong.
\end{enumerate}

The last point could be debated by the concept that the function of work is most likely consistent over the number of robots. It is simply some of its variables that change. Meaning that at any point in time the work done by three robots will be higher than the work done by two or one robot. This, of course, relies on the assumption that no negative work can be done. In such a small space as the provided testing arena, the robots frequently seemed to interfere with each other by physically impeding each other. This could be a major reason to disprove the assumption that no negative work can be done.

It is perhaps of interest that at least two of these issues are caused by the available hardware. A relatively small field and unreliable sensors meant the robot had difficulty detecting blocks and other robots; or exhibited incorrect behaviour due to ill-timed exceptions.

\subsection{Comparison with Dawson et al.}
There is one major difference between our own results and those reported in the paper by Dawson; while the performance of the lemmings performing the task increases better than linearly in the paper, our own results do not even show a linear performance increase. We've already discussed several possible reasons for this result.

The paper seems to suggest a major reason for increased performance by multiple robots is because a single lemming in their experiment tends to roam the sides of the arena, while having multiple lemmings forces them to visit the center (or at least visit the center more often).

As is evident from the pictures of our solo run, the lemming seems to have no problem visiting the center of the arena despite no other lemmings being present. There are two major explanations for this.
\begin{enumerate}
    \item The arena is physically smaller compared to lemming size than the one presented in the paper.
    \item The lemmings in the paper followed a tighter off-center offset during standard movement.
\end{enumerate}

With this main reason for added performance increase with a higher lemming count not applicable to our case, the issues as named in the last section (bad hardware) cause severe performance degradation in relation to what could be expected from the paper.